﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("Geta.Community.EntityAttributeBuilder")]
[assembly: AssemblyDescription("EPiServer Relate+ entity attribute builder library. Synchronizes EPiServer Relate+ entity's attributes defined in classes. Provides also strongly typed access to entity's attributes. Supports also strongly-typed quries.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Geta AS")]
[assembly: AssemblyProduct("Geta.Community.EntityAttributeBuilder")]
[assembly: AssemblyCopyright("Geta AS, Valdis Iljuconoks, 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("b052fdbe-4e1a-4299-86d6-fbf441a6628a")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("0.5")]
[assembly: AssemblyFileVersion("0.5")]

[assembly: InternalsVisibleTo("Geta.Community.EntityAttributeBuilder.Tests")]