﻿(function ($) {
    var controllerUrl = '/modules/geta.cachemanager/admin/cachemanagercontroller.ashx';
    var cacheManager = {
        table: null,
        cacheListTable: null,
        init: function () {
            cacheManager.table = $('[data-role="server-stats-table"]');
            cacheManager.cacheListTable = $('[data-role="cache-list-table"]');

            $('body')
                .on('click', '[data-role="clear-cache-button"]', function () {
                    cacheManager.clearCache();
                })
                .on('click', '[data-role="refresh-stats-button"]', function () {
                    cacheManager.refresh();
                })
                .on('click', '[data-role="cache-list-table"] .cm-checkbox-select-all', function () {
                    cacheManager.selectAllCacheItem($(this).is(':checked'));
                })
                .on('click', '[data-role="delete-selected-cache-button"]', function () {
                    cacheManager.deleteSelectedCacheItem();
                })
                .on('click', '[data-role="show-advanced-features"]', function (e) {
                    e.preventDefault();
                    var link = $(this);
                    var panel = $('.advanced-features');
                    console.log(panel);

                    if (cacheManager.cacheListTable.is(':visible')) {
                        panel.slideUp('fast', function () {
                            link.text(link.data('text'));
                        });
                    } else {
                        panel.slideDown('fast', function () {
                            cacheManager.getCacheList();
                            link.data('text', link.text()).text('Hide advanced features');
                        });
                    }
                });

            cacheManager.refresh();
        },

        refresh: function () {
            cacheManager.getInfo();
            if (cacheManager.cacheListTable.is(':visible')) {
                cacheManager.getCacheList();
            }
        },

        getInfo: function () {
            if (!cacheManager.table) {
                return;
            }
            $.getJSON(controllerUrl, { cmd: 'getinfo' }, function (data) {
                if (data != null) {
                    var tbody = cacheManager.table.find('tbody');
                    tbody
                        .find('tr:nth-child(1) td:nth-child(2)').html(data.MachineName).end()
                        .find('tr:nth-child(2) td:nth-child(2)').html(data.AvailableMemory + ' MB').end()
                        .find('tr:nth-child(3) td:nth-child(2)').html(data.ApplicationPath).end()
                        .find('tr:nth-child(4) td:nth-child(2)').html(data.AppCacheEntries);
                }
            });
        },

        clearCache: function () {
            $.getJSON(controllerUrl, { cmd: 'clearcache' }, function (data) {
                if (data != null) {
                    if (data) {
                        cacheManager.refresh();
                    }
                }
            });
        },

        getCacheList: function () {
            if (!cacheManager.cacheListTable) {
                return;
            }

            var tbody = cacheManager.cacheListTable.find('tbody');
            var rows = '';
            $.getJSON(controllerUrl, { cmd: 'getcachelist' }, function (data) {
                if (data != null) {
                    for (var i in data) {
                        rows += '<tr><td><input name="cacheItem" type="checkbox" value="' + data[i] + '"></td><td>' + data[i] + '</td></tr>';
                    }
                    tbody.html(rows);
                }
            }).error(function (jqXHR, textStatus, errorThrown) {
                tbody.html('<tr><td colspan="2"><p class=EP-systemMessage EP-systemMessage-None">' + errorThrown + '</p></td></tr>');
            });
        },

        selectAllCacheItem: function (select) {
            if (!cacheManager.cacheListTable) {
                return;
            }

            cacheManager.cacheListTable.find('input[type="checkbox"]').each(function () {
                this.checked = select;
            });
        },

        deleteSelectedCacheItem: function () {
            if (!cacheManager.cacheListTable) {
                return;
            }

            var checkboxes = cacheManager.cacheListTable.find('tbody:first').find('input[type="checkbox"]:checked');

            if (checkboxes.length) {
                $.ajax({
                    url: controllerUrl,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        cmd: 'deleteselectedcache',
                        cachelist: JSON.stringify($(checkboxes).serializeArray(), null, 2)
                    },
                    success: function (data) {
                        if (data != null) {
                            if (data) {
                                cacheManager.cacheListTable.find('input[type="checkbox"]').each(function () { this.checked = false; });
                                cacheManager.refresh();
                            }
                        }
                    }
                });
            }
        }
    };

    $(function () {
        cacheManager.init();
    });
})(jQuery);