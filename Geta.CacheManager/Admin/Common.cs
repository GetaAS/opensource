﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using System.Web.Caching;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Geta.CacheManager.Admin
{
    internal class Common
    {
        internal static MemoryStream BinarySerialize(object objectToSerialize)
        {
            MemoryStream serializationStream = new MemoryStream();
            new BinaryFormatter().Serialize(serializationStream, objectToSerialize);
            return serializationStream;
        }

        public static void ClearCache()
        {
            HttpContext current = HttpContext.Current;
            Cache cache = current != null ? current.Cache : HttpRuntime.Cache;
            ArrayList list = GetCacheList();
            foreach (object obj2 in list)
            {
                cache.Remove(obj2.ToString());
            }
        }

        public static ArrayList GetCacheList()
        {
            HttpContext current = HttpContext.Current;
            Cache cache = current != null ? current.Cache : HttpRuntime.Cache;
            var list = new ArrayList();
            IDictionaryEnumerator enumerator = cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                list.Add(enumerator.Key.ToString());
            }
            return list;
        }

        public static bool DeleteCacheItem(string item)
        {
            try
            {
                HttpContext current = HttpContext.Current;
                Cache cache = current != null ? current.Cache : HttpRuntime.Cache;
                cache.Remove(item);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool IsNullorEmpty(string text)
        {
            return ((text == null) || (text.Trim() == string.Empty));
        }

        internal static StringWriter XmlSerialize(object objectToSerialize)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(objectToSerialize.GetType());
                StringWriter textWriter = new StringWriter();
                XmlQualifiedName[] namespaces = new XmlQualifiedName[] { new XmlQualifiedName() };
                serializer.Serialize(textWriter, objectToSerialize, new XmlSerializerNamespaces(namespaces));
                return textWriter;
            }
            catch (InvalidOperationException)
            {
            }
            catch (SerializationException)
            {
            }
            catch (Exception exception)
            {
                string message = exception.Message;
            }
            return null;
        }

        public static void GetCacheInfo(HttpContext context)
        {
            HttpContext current = HttpContext.Current;
            if (current != null)
            {
                var result = new
                {
                    Environment.MachineName,
                    AvailableMemory = GetAvailableMemory(),
                    current.Request.ApplicationPath,
                    AppCacheEntries = current.Cache.Count.ToString(CultureInfo.InvariantCulture)
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }

        public static string GetAvailableMemory()
        {
            PerformanceCounter counter = null;

            try
            {
                counter = new PerformanceCounter("Memory", "Available MBytes");
                return counter.NextValue().ToString(CultureInfo.InvariantCulture);
            }
            catch
            {
                return "Error";
            }
            finally
            {
                if (counter != null)
                {
                    counter.Dispose();
                }
            }
        }

        public static void ClearCache(HttpContext context)
        {
            try
            {
                Common.ClearCache();
                context.Response.Write("true");
            }
            catch (Exception e)
            {
                context.Response.Write("false");
            }
        }

        public static void GetCacheList(HttpContext context)
        {
            try
            {
                ArrayList result = Common.GetCacheList();
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
            catch (Exception e)
            {
                context.Response.Write("false");
            }
        }

        public static void DeleteSelectedCache(HttpContext context, JArray cacheList)
        {
            try
            {
                foreach (JToken item in cacheList)
                {
                    Common.DeleteCacheItem(item["value"].ToString());
                }
                context.Response.Write("true");
            }
            catch (Exception e)
            {
                context.Response.Write("false");
            }
        }
    }
}