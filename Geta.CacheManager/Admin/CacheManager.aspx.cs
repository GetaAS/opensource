﻿using System;
using System.Globalization;
using System.Web;
using EPiServer.Security;
using EPiServer.UI;

namespace Geta.CacheManager.Admin
{
    public partial class CacheManager : SystemPageBase
    {
        public string MachineName { get; set; }

        public string AvailableMemory { get; set; }

        public string ApplicationPath { get; set; }

        public string AppCacheEntries { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!PrincipalInfo.HasAdminAccess)
            {
                AccessDenied();
            }

            HttpContext current = HttpContext.Current;
            if (current == null)
            {
                return;
            }

            MachineName = Environment.MachineName;
            AvailableMemory = Common.GetAvailableMemory();
            ApplicationPath = current.Request.ApplicationPath;
            AppCacheEntries = current.Cache.Count.ToString(CultureInfo.InvariantCulture);
        }
    }
}