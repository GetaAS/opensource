using System;
using System.Web;
using EPiServer.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Geta.CacheManager.Admin
{
    /// <summary>
    /// Handler class for Cache Manager. This is where requests are processed.
    /// </summary>
    public class CacheManagerController : IHttpHandler
    {
        #region IHttpHandler Members

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                if (!PrincipalInfo.HasAdminAccess)
                {
                    context.Response.StatusCode = 401;
                    context.Response.End();
                    return;
                }

                string cmd = context.Request.QueryString["cmd"];
                if (context.Request.RequestType == "POST")
                {
                    cmd = context.Request["cmd"];
                }

                if (cmd == "getinfo")
                {
                    Common.GetCacheInfo(context);
                }
                else if (cmd == "clearcache")
                {
                    Common.ClearCache(context);
                }
                else if (cmd == "getcachelist")
                {
                    Common.GetCacheList(context);
                }
                else if (cmd == "deleteselectedcache")
                {
                    Common.DeleteSelectedCache(context, JArray.Parse(context.Request["cachelist"]));
                }
            }
            catch (NotImplementedException ex)
            {
                context.Response.StatusCode = 501;
                context.Response.Write(JsonConvert.SerializeObject(ex.Message));
            }
        }

        public bool IsReusable
        {
            get { return false; }
        }

        #endregion
    }
}