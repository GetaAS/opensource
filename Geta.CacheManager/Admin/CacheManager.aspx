﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CacheManager.aspx.cs" Inherits="Geta.CacheManager.Admin.CacheManager" %>
<%@ Import Namespace="SquishIt.Framework" %>
<%@ Import Namespace="SquishIt.Framework.Minifiers.JavaScript" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Cache Manager</title>
        <link rel="stylesheet" type="text/css" href="/cms/Shell/ClientResources/ShellCore.css"/>
        <link rel="stylesheet" type="text/css" href="/cms/Shell/ClientResources/ShellCoreLightTheme.css"/>
        <link rel="stylesheet" type="text/css" href="/cms/Shell/ClientResources/EPi/Shell/Light/Shell.css"/>
        <link rel="stylesheet" type="text/css" href="/App_Themes/Default/Styles/ToolButton.css"/>
    </head>
    <body>
        <form id="form1" runat="server">
            <div class="epi-contentContainer epi-padding-large">
                <h1 class="EP-prefix">System Stats</h1>
                <table data-role="server-stats-table" class="epi-default">
                    <tbody>
                        <tr>
                            <td>Server Name</td>
                            <td><%= MachineName %></td>
                        </tr>
                        <tr>
                            <td>Free Memory</td>
                            <td><%= AvailableMemory %> MB</td>
                        </tr>
                        <tr>
                            <td>Application Path</td>
                            <td><%= ApplicationPath %></td>
                        </tr>
                        <tr>
                            <td>Application Cache Entry</td>
                            <td><%= AppCacheEntries %></td>
                        </tr>
                    </tbody>
                </table>
                <div class="epi-buttonDefault">
                    <span class="epi-cmsButton">
                        <input type="button" data-role="refresh-stats-button" class="epi-cmsButton-text epi-cmsButton-tools epi-cmsButton-Refresh" value="Refresh" />
                    </span>
                    <span class="epi-cmsButton">
                        <input type="button" data-role="clear-cache-button" class="epi-cmsButton-text epi-cmsButton-tools epi-cmsButton-Delete" value="Clear All Cache" />
                    </span>
                </div>
                <p>
                    <a href="#" data-role="show-advanced-features">Show advanced features</a>
                </p>
                <div class="advanced-features" style="display: none">
                    <h1 class="EP-prefix">Delete Cache Entries</h1>
                    <p>Please select the cache entries you wish to delete, and click the button below to delete.</p>
                    <div class="epi-buttonDefault">
                        <span class="epi-cmsButton">
                            <input type="button" data-role="delete-selected-cache-button" class="epi-cmsButton-text epi-cmsButton-tools epi-cmsButton-Delete" value="Delete Selected" />
                        </span>
                    </div>
                    <table data-role="cache-list-table" class="epi-default">
                        <thead>
                            <tr>
                                <th><input type="checkbox" class="cm-checkbox-select-all" /></th>
                                <th>Key</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"> </script>
        <script type="text/javascript"> window.jQuery || document.write(unescape("%3Cscript src='/Scripts/jquery-1.7.2.min.js' type='text/javascript'%3E%3C/script%3E")); </script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/json2/20110223/json2.min.js"> </script>
        <script type="text/javascript"> window.JSON || document.write(unescape("%3Cscript src='/Scripts/json2.min.js' type='text/javascript'%3E%3C/script%3E")); </script>
        <%= Bundle.JavaScript()
                            .Add("../Scripts/cachemanager.js")
                            .WithMinifier<YuiMinifier>()
                            .Render("../Scripts/cm_combined_#.js")
        %>
    </body>
</html>