﻿<%@ Page AutoEventWireup="true" CodeBehind="DdsAdmin.aspx.cs" Inherits="Geta.DdsAdmin.Admin.DdsAdmin" Language="C#" %>
<%@ Import Namespace="EPiServer.Data.Dynamic" %>
<%@ Import Namespace="SquishIt.Framework" %>
<%@ Import Namespace="SquishIt.Framework.Minifiers.JavaScript" %>
<%@ Import Namespace="SquishIt.Framework.Minifiers.CSS" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head runat="server">
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title><%= CurrentStoreName %></title>
        <asp:PlaceHolder runat="server">
            <%= Bundle.Css().WithMinifier<YuiCompressor>()
                            .Add("~/Content/DataTables-1.9.2/media/css/demo_page.css")
                            .Add("~/Content/DataTables-1.9.2/media/css/demo_table.css")
                            .Add("~/Content/DataTables-1.9.2/media/css/demo_table_jui.css")
                            .Add("~/Content/themes/DDSAdmin/custom/jquery-ui-1.8.20.custom.css")
                            .Add("~/Content/themes/DDSAdmin/main.css")
                            .Render("~/Content/themes/DDSAdmin/combined-#.css") %>

            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
            <script type="text/javascript">
                if (typeof jQuery == 'undefined') {
                    document.write(unescape("%3Cscript src='/Scripts/jquery-1.7.2.min.js' type='text/javascript'%3E%3C/script%3E"));
                }
            </script>
            <script src="//ajax.aspnetcdn.com/ajax/jquery.ui/1.8.20/jquery-ui.min.js" type="text/javascript"></script>
            <script type="text/javascript">
                if (typeof jQuery.ui == 'undefined') {
                    document.write(unescape("%3Cscript src='/Scripts/jquery-ui-1.8.20.min.js' type='text/javascript'%3E%3C/script%3E"));
                }
            </script>
            <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js" type="text/javascript"></script>
            <script type="text/javascript">
                if (typeof jQuery.validate == 'undefined') {
                    document.write(unescape("%3Cscript src='/Scripts/jquery.validate.min.js' type='text/javascript'%3E%3C/script%3E"));
                }
            </script>
        
            <%= Bundle.JavaScript().WithMinifier<YuiMinifier>()
                            .Add("~/Scripts/DataTables-1.9.2/media/js/jquery.dataTables.js")
                            .Add("~/Scripts/jquery.dataTables.editable.js")
                            .Add("~/Scripts/jquery.jeditable.js")
                            .Render("../Scripts/dds_combined_#.js") %>
        </asp:PlaceHolder>
    </head>
    <body>
        <asp:Panel runat="server" id="hdivNoStoreTypeSelected">
            <h3>No Store Type selected</h3>
        </asp:Panel>
        
        <asp:Panel runat="server" id="hdivStoreTypeDoesntExist" Visible="False">
            <h3>Selected Store Type does not exist</h3>
        </asp:Panel>

        <asp:Panel runat="server" id="hdivStoreTypeSelected">
            <form id="formAddNewRow" action="#" title="Add new <%= CurrentStoreName %>">
                <asp:repeater runat="server" ID="repForm" >
                    <HeaderTemplate>
                        <input type="text" name="form_Id" id="form_Id" rel="0" readonly="readonly" style="display: none" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <label for="form_<%# ((PropertyMap) Container.DataItem).PropertyName %>">
                            <%# ((PropertyMap) Container.DataItem).PropertyName %>
                            <input type="text" name="form_<%# ((PropertyMap) Container.DataItem).PropertyName %>"
                                   id="form_<%# ((PropertyMap) Container.DataItem).PropertyName %>" rel="<%# Container.ItemIndex + 1 %>" />
                        </label>
                    </ItemTemplate>
                </asp:repeater>
            </form>

            <h3>Selected Store Type: <%= CurrentStoreName %></h3>

            <table cellpadding="0" cellspacing="0" border="0" class="display" id="storeItems">
                <thead>
                    <tr>
                        <th>Id</th>
                        <asp:Repeater runat="server" ID="repColumnsHeader">
                            <ItemTemplate>
                                <th><%# ((PropertyMap) Container.DataItem).PropertyName %></th>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="2" class="dataTables_empty">Loading data from server</td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>

        <script type="text/javascript" charset="utf-8">
            $(function() {
                $('#storeItems').dataTable({
                    bJQueryUI: true,
                    bProcessing: true,
                    bServerSide: true,
                    sPaginationType: "full_numbers",
                    sAjaxSource: "Data.ashx?operation=read&store=<%= CurrentStoreName %>"
                }).makeEditable({
                    sUpdateURL: "Data.ashx?operation=update&store=<%= CurrentStoreName %>",
                    sAddURL: "Data.ashx?operation=create&store=<%= CurrentStoreName %>",
                    sAddHttpMethod: "POST",
                    sDeleteHttpMethod: "POST",
                    sDeleteURL: "Data.ashx?operation=delete&store=<%= CurrentStoreName %>",
                    oAddNewRowButtonOptions: {
                        label: "Add...",
                        icons: { primary: 'ui-icon-plus' }
                    },
                    oDeleteRowButtonOptions: {
                        label: "Remove",
                        icons: { primary: 'ui-icon-trash' }
                    },
                    oAddNewRowFormOptions: {
                        title: 'Add a new row to <%= CurrentStoreName %>',
                        modal: false,
                        width: 450
                    },
                    sAddDeleteToolbarSelector: ".dataTables_length",
                    sAddNewRowFormId: "formAddNewRow",
                    aoColumns: [<%= Columns %>]
                });
            });
        </script>
    </body>
</html>