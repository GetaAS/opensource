using System;
using System.Linq;
using EPiServer.Data.Dynamic;
using EPiServer.Security;
using EPiServer.UI;
using Geta.DdsAdmin.Dds;

namespace Geta.DdsAdmin.Admin
{
    public partial class DdsAdmin : SystemPageBase
    {
        protected string CurrentStoreName { get; set; }
        protected StoreInfo Store { get; set; }
        protected string Columns { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!PrincipalInfo.HasAdminAccess)
            {
                AccessDenied();
            }

            CurrentStoreName = Request.QueryString["Store"];

            if (!IsPostBack)
            {
                this.hdivStoreTypeDoesntExist.Visible = false;

                if (string.IsNullOrEmpty(CurrentStoreName))
                {
                    this.hdivNoStoreTypeSelected.Visible = true;
                    this.hdivStoreTypeSelected.Visible = false;
                    return;
                }

                this.hdivNoStoreTypeSelected.Visible = false;
                this.hdivStoreTypeSelected.Visible = true;

                var explorer = new Store();

                Store = explorer.Explore().FirstOrDefault(s => s.Name == CurrentStoreName);

                if (Store == null)
                {
                    this.hdivNoStoreTypeSelected.Visible = false;
                    this.hdivStoreTypeSelected.Visible = false;
                    this.hdivStoreTypeDoesntExist.Visible = true;
                    return;
                }

                this.repColumnsHeader.DataSource = Store.Columns;
                this.repForm.DataSource = Store.Columns;
                this.repColumnsHeader.DataBind();
                this.repForm.DataBind();
                this.Columns = GenerateColumnData(Store);
            }
        }

        private string GenerateColumnData(StoreInfo store)
        {
            // first column is Guid id so its always read only
            string result = @"null";

            // get other columns
            foreach (PropertyMap column in Store.Columns)
            {
                if (column.PropertyType == typeof (bool))
                {
                    result += @", {type: 'select', onblur: 'submit', data: ""{'True':'True', 'False':'False'}""}";
                    continue;
                }
                if (column.PropertyType == typeof (int))
                {
                    result += @", {cssclass: 'number'}";
                    continue;
                }
                result += @", {}"; // empty definition for to be treated as string
            }
            return result;
        }
    }
}